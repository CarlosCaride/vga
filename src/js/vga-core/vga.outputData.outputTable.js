/**
 * Clase que representa una tabla de datos de salida
 * @extends {TablePrimitive}
 */
class OutputTable extends TablePrimitive {
  /**
   * Constructor por defecto
   * @param {string} name Nombre del control
   * @param {string} label Etiqueta del control
   * @param {string} description Descripción del control
   * @param {TABLEHEADERTYPE} columnHeaderType Tipo de cabecera de las columnas
   * @param {string[]} columnHeaders Cabeceras personalizadas de las columnas
   * @param {TABLEHEADERTYPE} rowHeaderType Tipo de cabecera de las filas
   * @param {string[]} rowHeaders Cabeceras personalizadas de las filas
   * @param {object[][]} data Datos de la tabla
   */
  constructor(
    name,
    label,
    description,
    columnHeaderType,
    columnHeaders,
    rowHeaderType,
    rowHeaders,
    data
  ) {
    super(
      name,
      label,
      description,
      columnHeaderType,
      columnHeaders,
      rowHeaderType,
      rowHeaders,
      data
    );
  }

  /**
   * Actualiza el control
   */
  #updateControl() {
    if (this.getLabel() != undefined) {
      $("#" + this.getName() + "Label").text(this.getLabel());
    }

    if (this.getData() != undefined) {
      if (this.getRowHeaderType() == TABLEHEADERTYPE.CUSTOM) {
        $("#" + this.getName() + "_f" + i + "rh").val(
          this.getCustomRowHeaders()[i]
        );
      }
      for (var j = 0; j < this.getData()[i].length; j++) {
        $("#" + this.getName() + "_f" + i + "c" + j).text(this.getData()[i][j]);
      }
    }

    if (this.getDescription() != undefined) {
      $("#" + this.getName() + "Help").text(this.getDescription());
    }
  }

  /**
   * Obtiene el html de la tabla
   * @return El código html de la tabla
   */
  getHtmlCode() {
    var html = "";
    html +=
      '<div class="form-group row" id="' +
      this.getName() +
      'Div"><label id="' +
      this.getName() +
      'Label" for="' +
      this.getName() +
      '" class="col-sm-2 col-form-label">' +
      this.getLabel() +
      ':</label><div class="col-sm-10">';

    html +=
      '<table id="' +
      this.getName() +
      '" class="table table-striped table-hover text-center">';
    if (this.getColumnHeaderType() != TABLEHEADERTYPE.NONE) {
      html += '<thead class="thead-light"><tr>';
      if (this.getRowHeaderType() != TABLEHEADERTYPE.NONE) {
        if (this.getRowsTitle() == undefined) html += "<th>#</th>";
        else html += "<th>" + this.getRowsTitle() + "</th>";
      }
      if (this.getColumnHeaderType() == TABLEHEADERTYPE.NUMBER) {
        for (var i = 0; i < this.getData()[0].length; i++) {
          html += "<th>" + (i + 1) + "</th>";
        }
      }
      if (this.getColumnHeaderType() == TABLEHEADERTYPE.NUMBER0) {
        for (var i = 0; i < this.getData()[0].length; i++) {
          html += "<th>" + i + "</th>";
        }
      }
      if (this.getColumnHeaderType() == TABLEHEADERTYPE.CUSTOM) {
        for (var i = 0; i < this.getCustomColumnHeaders().length; i++) {
          if (this.getCustomColumnHeaders().length > i)
            html += "<th>" + this.getCustomColumnHeaders()[i] + "</th>";
        }
      }
      html += "</tr></thead>";
    }

    for (var i = 0; i < this.getData().length; i++) {
      html += "<tr>";
      if (this.getRowHeaderType() != TABLEHEADERTYPE.NONE) {
        if (this.getRowHeaderType() == TABLEHEADERTYPE.NUMBER) {
          html += "<th>" + (i + 1) + "</th>";
        }
        if (this.getRowHeaderType() == TABLEHEADERTYPE.NUMBER0) {
          html += "<th>" + i + "</th>";
        }
        if (
          this.getRowHeaderType() == TABLEHEADERTYPE.CUSTOM &&
          this.getCustomRowHeaders().length > i
        ) {
          html +=
            '<th id="' +
            this.getName() +
            "_f" +
            i +
            'rh">' +
            this.getCustomRowHeaders()[i] +
            "</th>";
        }
      }

      for (var j = 0; j < this.getData()[i].length; j++) {
        html +=
          '<td id="' +
          this.getName() +
          "_f" +
          i +
          "c" +
          j +
          '">' +
          (this.getData()[i][j] == undefined ? "" : this.getData()[i][j]) +
          "</td>";
      }
      html += "</tr>";
    }

    html += "</tr>";
    html += "</table>";
    html +=
      '<small id="' +
      this.getName() +
      'Help" class="form-text text-muted">' +
      this.getDescription() +
      "</small></div></div>";
    return html;
  }

  /**
   * Establece el valor indicado en la tabla
   * @param {number} row Número de fila
   * @param {number} column Número de columna
   * @param {object} value Valor
   */
  setValue(row, column, value) {
    this.getData()[(row, column)] = value;
    $("#" + this.getName() + "_f" + row + "c" + column).text(value);
  }

  /**
   * Selecciona la celda de la tabla
   * @param {number} row Número de fila
   * @param {number} column Número de columna
   */
  selectCell(row, column) {
    $("#" + this.getName() + "_f" + row + "c" + column).addClass(
      "textoCeldaRojo"
    );
  }

  /**
   * Deselecciona la celda de la tabla
   * @param {number} row Número de fila
   * @param {number} column Número de columna
   */
  unselectCell(row, column) {
    $("#" + this.getName() + "_f" + row + "c" + column).removeClass(
      "textoCeldaRojo"
    );
  }

  /**
   * Vacia el contenido de la tabla
   */
  clearData() {
    this.setData(undefined);
  }
}
