/**
 * Clase que representa una tabla de datos de entrada
 * @extends {TablePrimitive}
 */
class InputTable extends TablePrimitive {
  #staticTable;

  /**
   * Constructor por defecto
   * @param {string} name Nombre del control
   * @param {string} label Etiqueta del control
   * @param {string} description Descripción del control
   * @param {TABLEHEADERTYPE} columnHeaderType Tipo de cabecera de las columnas
   * @param {string[]} columnHeaders Cabeceras personalizadas de las columnas
   * @param {TABLEHEADERTYPE} rowHeaderType Tipo de cabecera de las filas
   * @param {string[]} rowHeaders Cabeceras personalizadas de las filas
   * @param {object[][]} data Datos de la tabla
   */
  constructor(
    name,
    label,
    description,
    columnHeaderType,
    columnHeaders,
    rowHeaderType,
    rowHeaders,
    data
  ) {
    super(
      name,
      label,
      description,
      columnHeaderType,
      columnHeaders,
      rowHeaderType,
      rowHeaders,
      data
    );
    this.#staticTable = false;
  }

  /**
   * Actualiza la tabla
   */
  #updateControl() {
    if (this.getLabel() != undefined) {
      $("#" + this.getName() + "Label").text(this.getLabel());
    }

    if (this.getData() != undefined) {
      for (var i = 0; i < this.getData().length; i++) {
        if (this.getRowHeaderType() == TABLEHEADERTYPE.CUSTOM) {
          $("#" + this.getName() + "_f" + i + "rh").val(
            this.getRowHeaders()[i]
          );
        }
        for (var j = 0; j < this.getData()[i].length; j++) {
          if (!this.#staticTable) {
            $("#" + this.getName() + "_f" + i + "c" + j + "_tb").val(
              this.getData()[i][j]
            );
          } else {
            $("#" + this.getName() + "_f" + i + "c" + j).text(
              this.getData()[i][j]
            );
          }
        }
      }
    }

    if (this.getDescription() != undefined) {
      $("#" + this.getName() + "Help").text(this.getDescription());
    }
  }

  /**
   * Obtiene el html de la tabla
   * @return El código html de la tabla
   */
  getHtmlCode() {
    var html = "";
    html +=
      '<div class="form-group row" id="' +
      this.getName() +
      'Div"><label id="' +
      this.getName() +
      'Label" for="' +
      this.getName() +
      '" class="col-sm-2 col-form-label">' +
      this.getLabel() +
      ':</label><div class="col-sm-10">';

    html +=
      '<table id="' +
      this.getName() +
      '" class="table table-striped table-hover text-center">';
    if (this.getColumnHeaderType() != TABLEHEADERTYPE.NONE) {
      html += '<thead class="thead-light"><tr>';
      if (this.getRowHeaderType() != TABLEHEADERTYPE.NONE) {
        if (this.getRowsTitle() == undefined) html += "<th>#</th>";
        else html += "<th>" + this.rowsTitle + "</th>";
      }
      if (this.getColumnHeaderType() == TABLEHEADERTYPE.NUMBER) {
        for (var i = 0; i < this.getData()[0].length; i++) {
          html += "<th>" + (i + 1) + "</th>";
        }
      }
      if (this.getColumnHeaderType() == TABLEHEADERTYPE.NUMBER0) {
        for (var i = 0; i < this.getData()[0].length; i++) {
          html += "<th>" + i + "</th>";
        }
      }
      if (this.getColumnHeaderType() == TABLEHEADERTYPE.CUSTOM) {
        for (var i = 0; i < this.getCustomColumnHeaders().length; i++) {
          if (this.getCustomColumnHeaders().length > i)
            html += "<th>" + this.getCustomColumnHeaders()[i] + "</th>";
        }
      }
      if (!this.#staticTable) {
        html += "<th>Acciones</th>";
      }
      html += "</tr></thead>";
    }

    for (var i = 0; i < this.getData().length; i++) {
      html += "<tr>";
      if (this.getRowHeaderType() != TABLEHEADERTYPE.NONE) {
        if (this.getRowHeaderType() == TABLEHEADERTYPE.NUMBER) {
          html += "<th>" + (i + 1) + "</th>";
        }
        if (this.getRowHeaderType() == TABLEHEADERTYPE.NUMBER0) {
          html += "<th>" + i + "</th>";
        }
        if (
          this.getRowHeaderType() == TABLEHEADERTYPE.CUSTOM &&
          this.getRowHeaders().length > i
        ) {
          html +=
            '<th id="' +
            this.getName() +
            "_f" +
            i +
            'rh"><input type="text" class="form-control tableInputStyle" id="' +
            this.getName() +
            "_f" +
            i +
            "c" +
            j +
            '_tb" value="' +
            this.getRowHeaders()[i] +
            '"></th>';
        }
      }

      for (var j = 0; j < this.getData()[i].length; j++) {
        html +=
          '<td id="' +
          this.getName() +
          "_f" +
          i +
          "c" +
          j +
          '"><input type="text" class="form-control tableInputStyle" id="' +
          this.getName() +
          "_f" +
          i +
          "c" +
          j +
          '_tb" value="' +
          this.getData()[i][j] +
          '"></td>';
      }
      if (!this.#staticTable) {
        html +=
          '<td id="' +
          this.getName() +
          "_f" +
          i +
          '">' +
          '<button type="button" class="btn btn-light btn-sm tableLink" onclick="InputData.get(\'' +
          this.getName() +
          "').deleteRow(" +
          (i + 1) +
          ')">Eliminar</button></td>';
      }
      html += "</tr>";
    }

    if (!this.#staticTable) {
      html += "<tr>";
      if (this.getRowHeaderType() != TABLEHEADERTYPE.NONE) {
        html += "<th></th>";
      }
      if (this.getColumnHeaderType() == TABLEHEADERTYPE.CUSTOM) {
        for (var i = 0; i < this.getCustomColumnHeaders().length; i++) {
          html += "<td></td>";
        }
      }
      if (this.getColumnHeaderType() == TABLEHEADERTYPE.NUMBER) {
        for (var i = 0; i < this.getData()[0].length; i++) {
          html += "<td></td>";
        }
      }
      html +=
        '<td><button type="button" class="btn btn-light btn-sm tableLink" onclick="InputData.get(\'' +
        this.getName() +
        "').addRow()\">Añadir fila</button></td>";
    }

    html += "</tr>";
    html += "</table>";
    html +=
      '<small id="' +
      this.getName() +
      'Help" class="form-text text-muted">' +
      this.getDescription() +
      "</small></div></div>";
    return html;
  }

  /**
   * Lee los datos de la tabla
   */
  readControl() {
    for (var i = 0; i < this.getData().length; i++) {
      if (this.getRowHeaderType() == TABLEHEADERTYPE.CUSTOM) {
        this.getRowHeaders()[i] = $(
          "#" + this.getName() + "_f" + i + "rh"
        ).val();
      }
      for (var j = 0; j < this.getData()[i].length; j++) {
        this.getData()[i][j] = $(
          "#" + this.getName() + "_f" + i + "c" + j + "_tb"
        ).val();
      }
    }
  }

  /**
   * Añade una nueva fila
   */
  addRow() {
    if (this.getRowHeaderType() == TABLEHEADERTYPE.CUSTOM) {
      this.getCustomRowHeaders().push("Nuevo elemento");
    }
    var elemento = new Array();
    for (var i = 0; i < this.getData()[0].length; i++) {
      elemento.push("Valor?");
    }
    this.getData().push(elemento);

    InputData.repaintControl(this);
  }

  /**
   * Elimina la fila indicada
   * @param {number} row Fila a eliminar
   */
  deleteRow(row) {
    row--;
    this.getData().splice(row, 1);

    if (this.getCustomRowHeaders() != undefined) {
      this.getCustomRowHeaders().splice(row, 1);
    }

    InputData.repaintControl(this);
  }

  /**
   * Establece si se puede añadir/eliminar filas a la tabla
   * @param {boolean} staticTable True si no se permite añadir/eliminar filas
   */
  setStatic(staticTable) {
    if (this.#staticTable == staticTable) return;
    this.#staticTable = staticTable;
    InputData.repaintControl(this);
  }
}
