/**
 * Clase gestora de los controles de datos de salia
 * @extends {ControlsContainer}
 */
class __OutputData extends ControlsContainer {
  #divOutputData;

  /**
   * Constructor por defecto
   * @constructor
   */
  constructor() {
    super();
    this.#divOutputData = "datosSalida";
  }

  /**
   * Añade un control a la salida de datos
   * @param {ControlPrimitive} control Control a añadir
   */
  addControl(control) {
    super.addControl(control);
    $("#" + this.#divOutputData).append(control.getHtmlCode());
  }

  /**
   * Repinta un control de la salia de datos
   * @param {ControlPrimitive} control Control a repintar
   */
  repaintControl(control) {
    $("#" + control.getName() + "Div").attr("id", "toDeleteDiv");
    $("#toDeleteDiv").before(control.getHtmlCode());
    $("#toDeleteDiv").remove();
  }

  /**
   * Limpia los resultados
   */
  clearResults() {
    var controls = this.getControls();
    if (controls == undefined) {
      return;
    }

    for (var i = 0; i < controls.length; i++) {
      controls[i].clearData();
    }
  }
}

/**
 * Instancia del gestor de los controles de salida de datos
 * @constant
 * @type {__OutputData}
 */
const OutputData = new __OutputData();
