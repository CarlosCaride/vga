/**
 * Clase que representa un grafo
 * @extends ControlPrimitive
 */
class Graph extends ControlPrimitive {
  #grafo;
  #layout;

  /**
   * Crea un nuevo grafo
   * @param {string} name Nombre del grafo
   * @param {string} label Etiqueta que se mostrará
   * @param {string} description Descripción que se mostrará
   * @constructor
   */
  constructor(name, label, description) {
    super(name, label, description);
  }

  /**
   * Retorna el html asociado a este control
   * @returns Código html del control
   */
  getHtmlCode() {
    return '<div id="' + this.getName() + 'Div" class="grafoDiv"></div>';
  }

  /**
   * Inicia el grafo con los estilos asociados
   * @param {object} styles Estilos del grafo
   */
  initGraph(styles) {
    this.#grafo = cytoscape({
      container: document.getElementById(this.getName() + "Div"), // container to render in
      style: styles,
    });
  }

  /**
   * Añade un nuevo nodo al grafo
   * @param {string} idNode Identificador del nodo
   * @param {srting} label Etiqueta del nodo
   */
  addNode(idNode, label) {
    this.#grafo.add([
      { data: { id: idNode, label: label }, classes: "multiline-auto" },
    ]);
  }

  /**
   * Añade una nueva arista al grafo
   * @param {string} idEdge Identificador de la arista
   * @param {string} idOrigin Identificador del nodo origen
   * @param {string} idDestiny Identificador del nodo destino
   * @param {string} label Etiqueta de la arista
   */
  addEdge(idEdge, idOrigin, idDestiny, label) {
    this.#grafo.add([
      {
        data: { id: idEdge, label: label, source: idOrigin, target: idDestiny },
      },
    ]);
  }

  /**
   * Obtiene el elemento del grafo
   * @param {string} idElement Identidicador del elemento
   * @returns El elemento del grafo
   */
  getElement(idElement) {
    this.#grafo.getElementById(idElement);
  }

  /**
   * Comprueba que existe un elemento dado en el grafo
   * @param {string} idElement Identificador del elemento
   * @returns true si el elemento exite, false en otro caso
   */
  checkIfExistElement(idElement) {
    return !(this.#grafo.getElementById(idElement).length == 0);
  }

  /**
   * Aplica un estilo a un elemento dado
   * @param {string} idElement Identificador del elemento
   * @param {string} style Nombre del estilo a aplicar
   */
  setStyleToElement(idElement, style) {
    this.#grafo.getElementById(idElement).addClass(style);
  }

  /**
   * Quita un estilo a un elemento dado
   * @param {string} idElement Identificador del elemento
   * @param {string} style Nombre del estilo a quitar
   */
  unsetStyleToElement(idElement, style) {
    this.#grafo.getElementById(idElement).removeClass(style);
  }

  /**
   * Aplica una nueva distribución al grafo
   * @param {Layout} layout Layout a aplicar
   */
  setLayout(layout) {
    this.#layout = layout;
    var l = layout.getLayout(this.#grafo);
    l.run();
  }

  /**
   * Devuelve la distribución actual
   * @returns El layout actual
   */
  getLayout() {
    return this.#layout;
  }

  /**
   * Refresca la distribución actual
   */
  refreshLayout() {
    this.#layout = layout;
    var l = this.#layout.getLayout(this.#grafo);
    l.run();
  }

  /**
   * Vacia el grafo
   */
  clearData() {
    this.#grafo.elements().remove();
  }
}

/**
 * Clase abstracta de una distribución de nodos en el grafo
 */
class Layout {
  #layoutType;

  /**
   * Constructor por defecto
   * @constructor
   * @param {string} layoutType Nombre de la distribución
   */
  constructor(layoutType) {
    this.#layoutType = layoutType;
  }

  /**
   * Obtiene el nombre de la distribución
   * @returns El nombre del layout
   */
  getLayoutType() {
    return this.#layoutType;
  }

  /**
   * Establece el nombre de la distribución
   * @param {string} layoutType Establece el nombre del layout
   */
  setLayoutType(layoutType) {
    this.#layoutType = layoutType;
  }
}

/**
 * Clase que representa una distribución aleatoria de los nodos del grafo
 * @extends Layout
 */
class RandomLayout extends Layout {
  /**
   * Constructor por defecto
   * @constructor
   */
  constructor() {
    super("random");
  }

  /**
   * Crea el layout del grafo
   * @param {Graph} graph Grafo al que se le creará el layout
   */
  getLayout(graph) {
    return graph.elements().layout({
      name: this.getLayoutType(),
    });
  }
}

/**
 * Clase que represeta la distribución en forma de tabla en un grafo
 * @extends Layout
 */
class GridLayout extends Layout {
  #rows;
  #columns;

  /**
   * Constructor por defecto
   * @constructor
   * @param {number} rows Número de filas
   * @param {number} columns Número de columnas
   */
  constructor(rows, columns) {
    super("grid");
    this.#rows = rows;
    this.#columns = columns;
  }

  /**
   * Retorna el número de filas de la distribución
   * @returns Número de filas
   */
  getRows() {
    return this.#rows;
  }

  /**
   * Establece el número de filas de la distribución
   * @param {number} rows Número de filas
   */
  setRows(rows) {
    this.#rows = rows;
  }

  /**
   * Retorna el número de columnas de la distribución
   * @returns Número de columnas
   */
  getColumns() {
    return this.#columns;
  }

  /**
   * Establece el número de columnas de la distribución
   * @param {number} rows Número de columnas
   */
  setColumns(columns) {
    this.#columns = columns;
  }

  /**
   * Crea el layout del grafo
   * @param {Graph} graph Grafo al que se le creará el layout
   */
  getLayout(graph) {
    return graph.elements().layout({
      name: this.getLayoutType(),
      rows: this.#rows,
      columns: this.#columns,
    });
  }
}

/**
 * Clase que represeta la distribución en forma de árbol en un grafo
 * @extends Layout
 */
class TreeLayout extends Layout {
  /**
   * Constructor por defecto
   * @constructor
   */
  constructor() {
    super("breadthfirst");
  }

  /**
   * Crea el layout del grafo
   * @param {Graph} graph Grafo al que se le creará el layout
   */
  getLayout(graph) {
    return graph.elements().layout({
      name: this.getLayoutType(),

      directed: true,

      nodeSep: undefined, // the separation between adjacent nodes in the same rank
      edgeSep: undefined, // the separation between adjacent edges in the same rank
      rankSep: undefined, // the separation between each rank in the layout
      rankDir: undefined, // 'TB' for top to bottom flow, 'LR' for left to right,
      ranker: undefined, // Type of algorithm to assign a rank to each node in the input graph. Possible values: 'network-simplex', 'tight-tree' or 'longest-path'
      minLen: function (edge) {
        return 1;
      }, // number of ranks to keep between the source and target of the edge
      edgeWeight: function (edge) {
        return 1;
      }, // higher weight edges are generally made shorter and straighter than lower weight edges

      // general layout options
      fit: true, // whether to fit to viewport
      padding: 30, // fit padding
      spacingFactor: undefined, // Applies a multiplicative factor (>0) to expand or compress the overall area that the nodes take up
      nodeDimensionsIncludeLabels: false, // whether labels should be included in determining the space used by a node
      animate: false, // whether to transition the node positions
      animateFilter: function (node, i) {
        return true;
      }, // whether to animate specific nodes when animation is on; non-animated nodes immediately go to their final positions
      animationDuration: 500, // duration of animation in ms if enabled
      animationEasing: undefined, // easing of animation if enabled
      boundingBox: undefined, // constrain layout bounds; { x1, y1, x2, y2 } or { x1, y1, w, h }
      transform: function (node, pos) {
        return pos;
      }, // a function that applies a transform to the final node position
      ready: function () {}, // on layoutready
      stop: function () {}, // on layoutstop
    });
  }
}
