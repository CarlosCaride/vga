/**
 * Clase gestora de los controles de datos de entrada
 * @extends {ControlsContainer}
 */
class __InputData extends ControlsContainer {
  #divInputData;
  #divUpdateData;
  #onUpdateFunction;

  /**
   * Constructor por defecto
   * @constructor
   */
  constructor() {
    super();
    this.#divInputData = "datosEntrada";
    this.#divUpdateData = "datosEntradaUpdate";
  }

  /**
   * Añade un control a la entrada de datos
   * @param {ControlPrimitive} control Control a añadir
   */
  addControl(control) {
    super.addControl(control);
    $("#" + this.#divUpdateData).before(control.getHtmlCode());
  }

  /**
   * Actualiza los datos de los controles
   */
  updateData() {
    FlowManager.reset();
    for (var i = 0; i < this.getControls().length; i++) {
      this.getControls()[i].readControl();
    }
    if (this.#onUpdateFunction != undefined) {
      this.#onUpdateFunction.call();
    }
  }

  /**
   * Ejecuta una acción al evento de actualizar los datos
   * @param {function} func Función a ejecutar al actualizar
   */
  onUpdate(func) {
    this.#onUpdateFunction = func;
  }
}

/**
 * Instancia del gestor de los controles de entrada de datos
 * @constant
 * @type {__InputData}
 */
const InputData = new __InputData();
