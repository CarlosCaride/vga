/**
 * Clase que representa un cuadro de texto de entrada
 * @extends {TextBoxPrimitive}
 */
class InputTextBox extends TextBoxPrimitive {
  /**
   * Constructor por defecto
   * @param {string} name Nombre del control
   * @param {string} label Etiqueta del control
   * @param {string} description Descripción del control
   * @param {object} data Valor del control
   */
  constructor(name, label, description, data) {
    super(name, label, description, data);
  }

  /**
   * Obtiene el html del control
   * @return El código html del control
   */
  getHtmlCode() {
    var val = "";
    if (this.getData() != undefined) {
      val = this.getData();
    }
    return (
      '<div class="form-group row" id="' +
      this.getName() +
      'Div"><label id="' +
      this.getName() +
      'Label" for="' +
      this.getName() +
      '" class="col-sm-2 col-form-label">' +
      this.getLabel() +
      ':</label><div class="col-sm-10"><input type="text" class="form-control" id="' +
      this.getName() +
      '" value="' +
      val +
      '"><small id="' +
      this.getName() +
      'Help" class="form-text text-muted">' +
      this.getDescription() +
      "</small></div></div>"
    );
  }

  /**
   * Lee los datos del control
   */
  readControl() {
    this.setData($("#" + this.getName()).val());
  }
}
