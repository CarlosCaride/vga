/**
 * Enumerado con los tipos de encabezado de tablas
 * @enum {number}
 */
const TABLEHEADERTYPE = {
  /**
   * Ningun encabezado
   */
  NONE: 0,
  /**
   * Encabezado de tipo numérico
   */
  NUMBER: 1,
  /**
   * Encabezado de tipo numérico base 0
   */
  NUMBER0: 2,
  /**
   * Encabezado personalizado
   */
  CUSTOM: 3,
};

/**
 * Clase que contiene distintas utilidades
 */
class Utilities {
  /**
   * Actualiza las fórmulas de la página
   */
  static refreshMath() {
    MathJax.Hub.Typeset();
  }

  /**
   * Devuelve un array como un string encerrado en corchetes { }
   * @param {object[]} array
   * @returns El array como string
   */
  static printArrayWithCurlyBraces(array) {
    if (array == null) {
      return "{}";
    }
    return "{" + array.toString() + "}";
  }

  /**
   * Devuelve un array como un string encerrado en corchetes { }
   * @param {object[]} array
   * @returns El array como string
   */
  static printArray(array) {
    if (array == null) {
      return "";
    }
    return array.toString();
  }

  /**
   * Devuelve un entero conprendido entre dos números
   * @param {int} min Menor entero
   * @param {int} max Mayor entero
   * @returns El entero seleccionado aleatoriamente
   */
  static getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  }

  /**
   * Devuelve un número conprendido entre otros dos
   * @param {int} min Menor número
   * @param {int} max Mayor número
   * @returns El número seleccionado aleatoriamente
   */
  static getRandomInt(min, max) {
    return Math.random() * (max - min) + min;
  }

  /**
   * Comprueba si existe el atributo indicado para el elemento id
   * @param {string} id Identifcador del elemento
   * @param {string} attr Atributo a comprobar
   * @returns Verdadero en caso de que contenga el atributo indicado
   */
  static hasAttr(id, attr) {
    var at = $(id).attr(attr);

    // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
    return typeof at !== typeof undefined && at !== false;
  }
}
