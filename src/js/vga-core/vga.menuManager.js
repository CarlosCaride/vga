/**
 * Clase de utilidades para la automatización de menús
 */
class __MenuManager {
  /**
   * Crea un menú de navegación entre las secciones de la página
   */
  createNavBar() {
    var list = $('<ul class="navbar-nav mr-auto">');
    $('h3,h4').each(function () {
      var id = $(this).attr('id');
      var text = $(this).attr('navtext');
      var item = $('<li class="nav-item">');
      var item = $('<li class="nav-item">');
      var link = $('<a class="nav-link" href="#' + id + '">' + text + '</a>');
      item.append(link);
      list.append(item);
    });
    $('#navBarAlgoritmo').append(list);
    $('#navBarAlgoritmoTitle').append($('h1').first().attr('navTitle'));
    $('#navBarAlgoritmoTitle').attr("href", "#" + $('h1').first().attr('id'))
  }
}

/**
 * Instancia de la clase de utilidad de automatización de menús
 * @constant
 * @type {__MenuManager}
 */
const MenuManager = new __MenuManager();
