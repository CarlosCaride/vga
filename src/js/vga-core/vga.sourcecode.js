/**
 * Clase que proporciona utilidades para la impresión de código fuente
 */
class __SourceCode {
  #lineaCodigo;
  #identPadding;

  #tokens;
  #simbolos;
  #codSimbolos;

  /**
   * Constructor por defecto
   * @constructor
   */
  constructor() {
    this.lineaCodigo = 0;
    this.identPadding = 20;

    this.tokens = new Array(0);
    this.simbolos = new Array(0);
    this.codSimbolos = new Array(0);
  }

  /**
   * Formatea los simbolos y tokens de una línea de código
   * @param {string} codigo Línea de código
   */
  #replaceSymbolsAndTokens(codigo) {
    var text, fLen, i;
    fLen = this.tokens.length;

    text = codigo;
    for (i = 0; i < fLen; i++) {
      if (text.search(this.tokens[i]) > -1) {
        text = text.replaceAll(this.tokens[i], "<b>" + this.tokens[i] + "</b>");
      }
    }

    fLen = this.simbolos.length;
    for (i = 0; i < fLen; i++) {
      if (text.search(this.simbolos[i]) > -1) {
        text = text.replaceAll(this.simbolos[i], this.codSimbolos[i]);
      }
    }

    return text;
  }

  /**
   * Establece las palabras reservadas del lenguaje
   * @param {string[]} tokens Array de palabras reservadas del lenguaje
   */
  setLanguageTokens(tokens) {
    this.tokens = tokens.sort(function (a, b) {
      return b.length - a.length;
    });
  }

  /**
   * Simbolos que se reemplazaran por los códigos dados
   * @param {string[]} simbolos Simbolos a modificar
   * @param {string[]} codigos Códigos de reemplado
   */
  setLanguageSymbols(simbolos, codigos) {
    this.simbolos = simbolos;
    this.codSimbolos = codigos;
  }

  /**
   * Imprime una línea de código en el bloque html reservado
   * @param {string} codigo Línea de código fuente
   * @param {number} indentado Número de identado de la línea
   */
  printCodeLine(codigo, indentado) {
    var cadena = this.#replaceSymbolsAndTokens(codigo);
    $("#algoritmo").append(
      '<div class="codigo" id="lineaCodigo' +
        this.lineaCodigo +
        '" style="margin-left: ' +
        this.identPadding * indentado +
        'px;">' +
        cadena +
        "</div>"
    );
    this.lineaCodigo++;
  }

  /**
   * Imprime una línea de código en el bloque html indicado
   * @param {string} codigo Línea de código fuente
   * @param {number} indentado Número de identado de la línea
   * @param {string} div Elemento donde se imprimirá el codigo
   */
  printAuxLineCode(codigo, indentado, div) {
    var cadena = this.#replaceSymbolsAndTokens(codigo);
    $("#" + div).append(
      '<div class="codigo" style="margin-left: ' +
        this.identPadding * indentado +
        'px;">' +
        cadena +
        "</div>"
    );
    this.lineaCodigo++;
  }
}

/**
 * Instancia del gestor de código fuente
 * @type {__SourceCode}
 * @constant
 */
const SourceCode = new __SourceCode();
