nombreTablaResultados = "tablaResultados";

capacidadMochila = 10;
objetosCandidatos = [
  [1, 2],
  [5, 14],
  [7, 15],
  [4, 10],
  [3, 5],
];

objetosSeleccionados = new Array(0);
beneficioTotal = 0;

function inicializar() {
  inicializarZonaVariables();

  codigoAlgoritmo();
  inicializarZonaResultados();
  ejecutarAlgoritmo();
}

//Imprime el algoritmo
function codigoAlgoritmo() {
  SourceCode.setLanguageTokens([
    "tipo",
    "fun",
    "ffun",
    "var",
    "fvar",
    "para",
    "hasta",
    "hacer",
    "fpara",
    "mientras",
    "fmientras",
    "dev",
    "sino",
    "si",
    "fsi",
    "incremento",
  ]);
  SourceCode.setLanguageSymbols(["<--"], ["&larr;"]);
  SourceCode.printCodeLine("tipo Tabla = matriz[0..n,0..V] de entero", 0);
  SourceCode.printCodeLine("tipo Vector = matriz[0..n] de entero", 0);
  SourceCode.printCodeLine("<br/>", 0);
  SourceCode.printCodeLine(
    "fun MochilaEntera(vol:Vector, ben:Vector, n:entero, V:entero, M:Tabla)",
    0
  );
  SourceCode.printCodeLine("var", 1);
  SourceCode.printCodeLine("i,j: entero", 2);
  SourceCode.printCodeLine("fvar", 1);
  SourceCode.printCodeLine("para i <-- 1 hasta n hacer", 1);
  SourceCode.printCodeLine("M[i,0] <-- 0", 2);
  SourceCode.printCodeLine("fpara", 1);
  SourceCode.printCodeLine("para j <-- 1 hasta V hacer", 1);
  SourceCode.printCodeLine("M[0,j] <-- 0", 2);
  SourceCode.printCodeLine("fpara", 1);
  SourceCode.printCodeLine("para i <-- 1 hasta n hacer", 1);
  SourceCode.printCodeLine("para j <-- 1 hasta V hacer", 2);
  SourceCode.printCodeLine("si vol[i] > j entonces", 3);
  SourceCode.printCodeLine("M[i,j] <-- M[i-1,j]", 4);
  SourceCode.printCodeLine("sino", 3);
  SourceCode.printCodeLine(
    "M[i,j] <-- max(M[i-1,j], M[i-1, j-vol[i]] + ben [i]})",
    4
  );
  SourceCode.printCodeLine("fsi", 3);
  SourceCode.printCodeLine("fpara", 2);
  SourceCode.printCodeLine("fpara", 1);
  SourceCode.printCodeLine("ffun", 0);
  SourceCode.printCodeLine("<br/>", 0);

  SourceCode.printCodeLine(
    "fun ObjetosEnMochila(vol:Vector, M:Tabla, n:entero, V:entero, objetos:Vector)",
    0
  );
  SourceCode.printCodeLine("var", 1);
  SourceCode.printCodeLine("i,W: entero", 2);
  SourceCode.printCodeLine("fvar", 1);
  SourceCode.printCodeLine("W <-- V", 1);
  SourceCode.printCodeLine("para i <-- n hasta 1 incremento -1 hacer", 1);
  SourceCode.printCodeLine("si M[i,W] = M[i-,W] entonces", 2);
  SourceCode.printCodeLine("objetos[i] <-- 0", 3);
  SourceCode.printCodeLine("sino", 2);
  SourceCode.printCodeLine("objetos[i] <-- 1", 3);
  SourceCode.printCodeLine("W <-- W - vol[i]", 3);
  SourceCode.printCodeLine("fsi", 2);
  SourceCode.printCodeLine("fpara", 1);
  SourceCode.printCodeLine("ffun", 0);
}

//Ejecuta el algoritmo
function ejecutarAlgoritmo() {
  FlowManager.startFlow();
  FlowManager.startStep();

  FlowManager.selectSourceLine(3);
  FlowManager.finishStep();

  beneficioTotal = 0;

  var objetos = new Array(objetosCandidatos.length + 1);

  for (var i = 0; i < objetosCandidatos.length; i++) {
    objetos[i + 1] = {
      vol: eval(objetosCandidatos[i][0]),
      ben: eval(objetosCandidatos[i][1]),
    };
  }

  var tabla = new Array(objetos.length);

  for (var i = 0; i < objetos.length; i++) {
    tabla[i] = new Array(capacidadMochila + 1);
    for (var j = 0; j <= capacidadMochila; j++) {
      tabla[i][j] = null;
    }
  }

  FlowManager.startStep();
  FlowManager.selectSourceLine(7);
  FlowManager.finishStep();
  FlowManager.startStep();

  for (var i = 0; i < objetos.length; i++) {
    FlowManager.oldCellValue("resultadosTabla", i.toString(), 0, tabla[i][0]);
    tabla[i][0] = 0;
    FlowManager.newCellValue("resultadosTabla", i.toString(), 0, tabla[i][0]);
  }

  FlowManager.selectSourceLine(8);
  FlowManager.finishStep();
  FlowManager.startStep();
  FlowManager.selectSourceLine(10);
  FlowManager.finishStep();
  FlowManager.startStep();

  for (var j = 0; j <= capacidadMochila; j++) {
    FlowManager.oldCellValue("resultadosTabla", 0, j, tabla[0][j]);
    tabla[0][j] = 0;
    FlowManager.newCellValue("resultadosTabla", 0, j, tabla[0][j]);
  }
  FlowManager.selectSourceLine(11);
  FlowManager.finishStep();

  for (var i = 1; i < objetos.length; i++) {
    for (var j = 1; j <= capacidadMochila; j++) {
      FlowManager.startStep();
      FlowManager.selectCell("resultadosTabla", i, j);
      FlowManager.oldCellValue("resultadosTabla", i, j, tabla[i][j]);
      var objeto = objetos[i];
      if (objeto.vol > j) {
        FlowManager.selectSourceLine(16);
        tabla[i][j] = tabla[i - 1][j];
      } else {
        FlowManager.selectSourceLine(18);
        tabla[i][j] = Math.max(
          tabla[i - 1][j],
          tabla[i - 1][j - objeto.vol] + objeto.ben
        );
      }
      FlowManager.newCellValue("resultadosTabla", i, j, tabla[i][j]);
      FlowManager.finishStep();
    }
  }

  var w = capacidadMochila;

  objetosSeleccionados = new Array(0);

  FlowManager.startStep();
  FlowManager.selectSourceLine(29);
  FlowManager.finishStep();

  for (var i = objetos.length - 1; i > 0; i--) {
    FlowManager.startStep();
    if (tabla[i][w] != tabla[i - 1][w]) {
      FlowManager.selectSourceLine(33);
      FlowManager.selectSourceLine(34);
      FlowManager.newCellValue("resultadosVector", 0, i - 1, 1);
      FlowManager.oldCellValue("resultadosVector", 0, i - 1, null);
      FlowManager.selectCellPermanent("resultadosTabla", i, w);
      FlowManager.selectCellPermanent("resultadosVector", 0, i - 1);
      objetosSeleccionados.push(objetos[i].vol);
      beneficioTotal += objetos[i].ben;
      w -= objetos[i].vol;
    } else {
      FlowManager.selectSourceLine(31);
      FlowManager.newCellValue("resultadosVector", 0, i - 1, 0);
      FlowManager.oldCellValue("resultadosVector", 0, i - 1, null);
    }
    FlowManager.finishStep();
  }

  objetosSeleccionados.reverse();

  FlowManager.startStep();
  FlowManager.selectSourceLine(37);

  FlowManager.oldResultValue("resultadosVolumen", null);
  FlowManager.oldResultValue("resultadosBeneficio", null);
  FlowManager.newResultValue("resultadosVolumen", objetosSeleccionados);
  FlowManager.newResultValue("resultadosBeneficio", beneficioTotal);

  FlowManager.finishStep();
  FlowManager.finishFlow();
}

//actualiza los nuevos datos al pulsar el boton actualizar
function actualizarDatos() {
  capacidadMochila = eval(InputData.get("capacidadMochila").getData());
  objetosCandidatos = InputData.get("entradaTabla").getData();

  OutputData.clearResults();
  var filas = new Array(objetosCandidatos.length + 1);
  filas[0] = "posición 0";
  var datos = new Array(objetosCandidatos.length);
  for (var i = 0; i < objetosCandidatos.length + 1; i++) {
    datos[i] = new Array(capacidadMochila + 1);
    if (i < objetosCandidatos.length)
      filas[i + 1] =
        "$v_" +
        (i + 1) +
        " = " +
        objetosCandidatos[i][0] +
        "$ $b_" +
        (i + 1) +
        " = " +
        objetosCandidatos[i][1] +
        "$";
  }
  OutputData.get("resultadosTabla").setCustomRowHeaders(filas);
  OutputData.get("resultadosTabla").setData(datos);
  OutputData.get("resultadosVector").setData([
    new Array(objetosCandidatos.length),
  ]);
  ejecutarAlgoritmo();
  Utilities.refreshMath();
}

//Inicia las variables
function inicializarZonaVariables() {
  var controlCapacidad = new InputTextBox(
    "capacidadMochila",
    "Tamaño de la mochila",
    "Volumen máximo de la mochila. La suma de los objetos no puede ser superior a este valor",
    capacidadMochila
  );
  InputData.addControl(controlCapacidad);

  var tabla = new InputTable(
    "entradaTabla",
    "Objetos candidatos",
    "Tabla de objetos que son candidatos a introducirse en la mochila.",
    TABLEHEADERTYPE.CUSTOM,
    ["Volumen", "Beneficio"],
    TABLEHEADERTYPE.NUMBER,
    null,
    objetosCandidatos
  );
  InputData.addControl(tabla);

  InputData.onUpdate(actualizarDatos);
}

//Inicia para este caso la tabla, el objetos de volumen y el beneficio
function inicializarZonaResultados() {
  var filas = new Array(objetosCandidatos.length + 1);

  filas[0] = "posición 0";

  var datos = new Array(objetosCandidatos.length);

  for (var i = 0; i < objetosCandidatos.length + 1; i++) {
    datos[i] = new Array(capacidadMochila + 1);
    if (i < objetosCandidatos.length)
      filas[i + 1] =
        "$v_" +
        (i + 1) +
        " = " +
        objetosCandidatos[i][0] +
        "$ $b_" +
        (i + 1) +
        " = " +
        objetosCandidatos[i][1] +
        "$";
  }

  var tabla = new OutputTable(
    "resultadosTabla",
    "Tabla de valores",
    "Tabla de datos usada por el algoritmo.",
    TABLEHEADERTYPE.NUMBER0,
    null,
    TABLEHEADERTYPE.CUSTOM,
    filas,
    datos
  );
  OutputData.addControl(tabla);

  var vectorObjetos = new OutputTable(
    "resultadosVector",
    "Objetos",
    "Vector de objetos que son elegidos para introducirse en la mochila.",
    TABLEHEADERTYPE.NUMBER,
    null,
    TABLEHEADERTYPE.CUSTOM,
    ["Seleccionado"],
    [new Array(objetosCandidatos.length)]
  );
  vectorObjetos.setRowsTitle("Índice objeto");

  OutputData.addControl(vectorObjetos);

  var controlVolumenRes = new OutputTextBox(
    "resultadosVolumen",
    "Volumenes seleccionados",
    "Volumenes de los objetos que se introduciran en la mochila.",
    null
  );
  OutputData.addControl(controlVolumenRes);

  var controlBeneficioRes = new OutputTextBox(
    "resultadosBeneficio",
    "Beneficio",
    "Beneficio obtenido con los objetos selccionados, que es máximo.",
    null
  );
  OutputData.addControl(controlBeneficioRes);
}
