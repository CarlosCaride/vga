datosEjemplo1 = [7, 8, 4, 2, 10, 1, 9, 6, 5, 3];
datosSalida = null;
datosEntrada = null;
pivot_base1 = null;

function inicializar() {
  inicializarZonaVariables();

  codigoAlgoritmo();
  inicializarZonaResultados();
  ejecutarAlgoritmo();
}

//Imprime el algoritmo
function codigoAlgoritmo() {
  SourceCode.setLanguageTokens([
    "tipo",
    "fun",
    "ffun",
    "var",
    "fvar",
    "para",
    "hasta",
    "hacer",
    "fpara",
    "mientras",
    "fmientras",
    "dev",
    "si",
    "sino",
    "fsi",
    "incremento",
    "repetir",
    "frepetir",
  ]);
  SourceCode.setLanguageSymbols(["<--"], ["&larr;"]);
  SourceCode.printCodeLine("fun Qicksort (T:vector [i..j] de entero)", 0); //0
  SourceCode.printCodeLine("var", 1); //1
  SourceCode.printCodeLine("pivote:natural", 2); //2
  SourceCode.printCodeLine("k:natural", 2); //3
  SourceCode.printCodeLine("menores:vector", 2); //4
  SourceCode.printCodeLine("iguales:vector", 2); //5
  SourceCode.printCodeLine("mayores:vector", 2); //6
  SourceCode.printCodeLine("fvar", 1); //7
  SourceCode.printCodeLine("si trivial(T) entonces dev T", 1); //8
  SourceCode.printCodeLine("sino", 1); //9
  SourceCode.printCodeLine("pivote <-- rand(i,j)", 2); //10
  SourceCode.printCodeLine("repetir k <-- k + 1 mientras k <= j", 2); //11
  SourceCode.printCodeLine("si k = pivote entonces continuar", 3); //12
  SourceCode.printCodeLine("sino", 3); //13
  SourceCode.printCodeLine(
    "si T[k] < T[pivote] entonces Insertar(menores, T[k])",
    4
  ); //14
  SourceCode.printCodeLine(
    "si T[k] > T[pivote] entonces Insertar(mayores, T[k])",
    4
  ); //15
  SourceCode.printCodeLine("sino Insertar(iguales, T[k])", 4); //16
  SourceCode.printCodeLine("frepetir", 2); //17
  SourceCode.printCodeLine(
    "dev [Qicksort(menores), iguales, Qicksort(mayores)]",
    2
  ); //18
  SourceCode.printCodeLine("fsi", 1); //19
  SourceCode.printCodeLine("ffun", 0); //20
}

//Ejecuta el algoritmo
function ejecutarAlgoritmo() {
  FlowManager.startFlow();
  FlowManager.startStep();
  FlowManager.selectSourceLine(0);
  FlowManager.finishStep();
  var data = InputData.get("datosSinOrden").getData();
  if (Array.isArray(data)) {
    quicksort(data, 0);
  } else {
    quicksort(data.split(","), 0);
  }
  FlowManager.finishFlow();
}

function quicksort(array, base) {
  FlowManager.startStep();
  FlowManager.selectSourceLine(0);
  for (var i = 0; i < array.length; i++) {
    FlowManager.unselectBar("grafica", i + base);
    FlowManager.selectBarPermanent("grafica", i + base, "GoldenRod");
  }
  FlowManager.finishStep();
  FlowManager.startStep();
  FlowManager.selectSourceLine(8);
  FlowManager.finishStep();
  if (array.length <= 1) {
    FlowManager.startStep();
    FlowManager.selectSourceLine(20);
    for (var i = 0; i < array.length; i++) {
      FlowManager.unselectBar("grafica", i + base);
    }
    FlowManager.finishStep();
    return array;
  }
  FlowManager.startStep();
  FlowManager.selectSourceLine(10);
  FlowManager.oldResultValue("pivote", pivot_base1);

  var pivot = Math.floor(Math.random() * Math.floor(array.length - 1));
  pivot_base1 = pivot;

  FlowManager.newResultValue("pivote", array[pivot]);
  FlowManager.selectBarPermanent("grafica", pivot + base, "purple");
  FlowManager.oldResultValue("smaller", Utilities.printArray(smaller));
  FlowManager.oldResultValue("middle", Utilities.printArray(middle));
  FlowManager.oldResultValue("bigger", Utilities.printArray(bigger));
  var smaller = new Array(0);
  var middle = [array[pivot]];
  var bigger = new Array(0);
  FlowManager.newResultValue("smaller", Utilities.printArray(smaller));
  FlowManager.newResultValue("middle", Utilities.printArray(middle));
  FlowManager.newResultValue("bigger", Utilities.printArray(bigger));

  FlowManager.finishStep();

  for (let i = 0; i < array.length; i++) {
    FlowManager.startStep();
    if (i != pivot) {
      FlowManager.selectBarPermanent("grafica", i + base, "orange");
    }
    if (i == pivot) {
      FlowManager.selectSourceLine(12);
    } else if (eval(array[i] - array[pivot]) == 0) {
      FlowManager.selectSourceLine(16);
      FlowManager.oldResultValue("middle", Utilities.printArray(middle));
      middle.push(array[i]);
      FlowManager.newResultValue("middle", Utilities.printArray(middle));
    } else if (eval(array[i] - array[pivot]) < 0) {
      FlowManager.selectSourceLine(14);
      FlowManager.oldResultValue("smaller", Utilities.printArray(smaller));
      smaller.push(array[i]);
      FlowManager.newResultValue("smaller", Utilities.printArray(smaller));
      FlowManager.selectBarPermanent("grafica", i + base, "green");
    } else {
      FlowManager.selectSourceLine(15);
      FlowManager.oldResultValue("bigger", Utilities.printArray(bigger));
      bigger.push(array[i]);
      FlowManager.newResultValue("bigger", Utilities.printArray(bigger));
      FlowManager.selectBarPermanent("grafica", i + base, "red");
    }
    FlowManager.finishStep();
  }
  //Rehacemos el gráfico
  FlowManager.startStep();
  FlowManager.selectSourceLine(18);
  FlowManager.oldResultValue(
    "datosOrdenados",
    Utilities.printArray(datosSalida)
  );
  var datosSalidaAux;
  if (base == 0){
    datosSalidaAux = [...smaller, ...middle, ...bigger, ...datosSalida.slice(smaller.length + middle.length + bigger.length)];
  } else if((base + smaller.length + middle.length + bigger.length) >= datosSalida.length) {
    datosSalidaAux = [...datosSalida.slice(0, base),
      ...smaller, ...middle, ...bigger];
  } else {
    datosSalidaAux = [...datosSalida.slice(0, base),
      ...smaller, ...middle, ...bigger, ...datosSalida.slice(base + smaller.length + middle.length + bigger.length)];
  }
  FlowManager.newResultValue(
    "datosOrdenados",
    Utilities.printArray(datosSalidaAux)
  );

  for (var i = 0; i < smaller.length; i++) {
    FlowManager.oldBarValue("grafica", i + base, datosSalida[i + base])
    FlowManager.newBarValue("grafica", i + base, datosSalidaAux[i + base])
  }
  for (var i = 0; i < middle.length; i++) {
    FlowManager.oldBarValue("grafica", i + base + smaller.length, datosSalida[i + base + smaller.length])
    FlowManager.newBarValue("grafica", i + base + smaller.length, datosSalidaAux[i + base + smaller.length])
  }
  for (var i = 0; i < bigger.length; i++) {
    FlowManager.oldBarValue("grafica", i + base + smaller.length + middle.length, datosSalida[i + base + smaller.length + middle.length])
    FlowManager.newBarValue("grafica", i + base + smaller.length + middle.length, datosSalidaAux[i + base + smaller.length + middle.length])
  }
  datosSalida = datosSalidaAux;
  FlowManager.finishStep();
  return [
    ...quicksort(smaller, base),
    ...middle,
    ...quicksort(bigger, base + smaller.length + middle.length),
  ];
}

//Inicia las variables
function inicializarZonaVariables() {
  var input = new InputTextBox(
    "datosSinOrden",
    "Datos",
    'Datos a ordenar (<a href="https://pinetools.com/es/generador-numeros-aleatorios" target="_blank">Generador</a>)',
    datosEjemplo1
  );
  InputData.addControl(input);

  InputData.onUpdate(actualizarDatos);
}

//Inicia para este caso la tabla, el objetos de volumen y el beneficio
function inicializarZonaResultados() {
  var chart = new BarChart(
    "grafica",
    "Gráfica",
    "Gráfica que representa los datos"
  );
  OutputData.addControl(chart);
  if (datosEntrada == null) {
    datosEntrada = datosEjemplo1;
  }
  chart.initChart(datosEntrada);

  var pivote = new OutputTextBox(
    "pivote",
    "Pivote",
    "Pivote actual. Se usa la función rand",
    ""
  );
  OutputData.addControl(pivote);
  var menores = new OutputTextBox(
    "smaller",
    "Menores",
    "Datos menores a pivote",
    ""
  );
  OutputData.addControl(menores);
  var iguales = new OutputTextBox(
    "middle",
    "Iguales",
    "Datos iguales a pivote",
    ""
  );
  OutputData.addControl(iguales);
  var mayores = new OutputTextBox(
    "bigger",
    "Mayores",
    "Datos mayores a pivote",
    ""
  );
  OutputData.addControl(mayores);

  var out = new OutputTextBox(
    "datosOrdenados",
    "Vector salida",
    "Vector con los datos de salida",
    Utilities.printArray(datosEjemplo1)
  );
  OutputData.addControl(out);
  datosSalida = datosEntrada;
}

//actualiza los datos
function actualizarDatos() {
  OutputData.clearResults();

  var elementos = InputData.get("datosSinOrden").getData().split(",");
  datosEntrada = new Array();
  for (var i = 0; i < elementos.length; i++) {
    datosEntrada.push(eval(elementos[i]));
  }
  OutputData.get("grafica").initChart(datosEntrada);
  datosSalida = datosEntrada;

  ejecutarAlgoritmo();
}
