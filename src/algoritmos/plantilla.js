
function inicializar() {
  inicializarZonaVariables();

  codigoAlgoritmo();
  inicializarZonaResultados();
  ejecutarAlgoritmo();
}

//Imprime el algoritmo
function codigoAlgoritmo() {

}

//Ejecuta el algoritmo
function ejecutarAlgoritmo() {

}

//Inicia las variables
function inicializarZonaVariables() {
  InputData.onUpdate(actualizarDatos);
}

//Inicia para este caso la tabla, el objetos de volumen y el beneficio
function inicializarZonaResultados() {
  
}

//actualiza los datos
function actualizarDatos() {
  OutputData.clearResults();

  ejecutarAlgoritmo();
}