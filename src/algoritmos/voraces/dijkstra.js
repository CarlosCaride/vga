grafo = null;
idRobot = null;
idMeta = null;
iM = null;
jM = null;
iR = null;
jR = null;

ejemploRobot1 = [
  ["O", "S", "O", "2", "1"],
  ["3", "1", "3", "1", "1"],
  ["1", "6", "6", "O", "6"],
  ["1", "2", "O", "R", "4"],
  ["7", "1", "1", "2", "6"],
];

function inicializar() {
  inicializarZonaVariables();

  codigoAlgoritmo();
  codigoAlgoritmoAuxiliar();
  inicializarZonaResultados();
  ejecutarAlgoritmo();
}

//Imprime el algoritmo
function codigoAlgoritmo() {
  SourceCode.setLanguageTokens([
    "tipo",
    "fun",
    "ffun",
    "var",
    "fvar",
    "para",
    "hasta",
    "hacer",
    "fpara",
    "mientras",
    "fmientras",
    "dev",
    "si",
    "fsi",
    "incremento",
  ]);
  SourceCode.setLanguageSymbols(["<--"], ["&larr;"]);
  SourceCode.printCodeLine("tipo VectorNat = matriz[O..n] de natural", 0);
  SourceCode.printCodeLine("<br/>", 0);
  SourceCode.printCodeLine(
    "fun Dijkstra (G = <N,A>: grafo, R: natural, S: natural): VectorNat, VectorNat",
    0
  );
  SourceCode.printCodeLine("var", 1);
  SourceCode.printCodeLine("especial, predecesor: VectorNat", 2);
  SourceCode.printCodeLine("C: conjunto de nodos", 2);
  SourceCode.printCodeLine("fvar", 1);
  SourceCode.printCodeLine("C={1,2,3,...,n} excepto R", 1);
  SourceCode.printCodeLine("para i = 1 hasta n y i ≠ R hacer", 1);
  SourceCode.printCodeLine("especial[i] = Distancia(R ,i)", 2);
  SourceCode.printCodeLine("predecesor[i] = R", 2);
  SourceCode.printCodeLine("fpara", 1);
  SourceCode.printCodeLine("mientras C contenga al nodo S hacer", 1);
  SourceCode.printCodeLine("v = nodo en C que minimiza especial[v]", 2);
  SourceCode.printCodeLine("C = C\\{v}", 2);
  SourceCode.printCodeLine("si v ≠ S entonces", 2);
  SourceCode.printCodeLine("para cada w en C hacer", 3);
  SourceCode.printCodeLine(
    "si especial[w] > especial[v] + Distancia(v,w) entonces",
    4
  );
  SourceCode.printCodeLine("especial[w] = especial[v] + Distancia(v,w)", 5);
  SourceCode.printCodeLine("predesecor[w] = v", 5);
  SourceCode.printCodeLine("fsi", 4);
  SourceCode.printCodeLine("fpara", 3);
  SourceCode.printCodeLine("fsi", 2);
  SourceCode.printCodeLine("fmientras", 1);
  SourceCode.printCodeLine("dev especial[],predecesor[]", 1);
  SourceCode.printCodeLine("ffun", 0);
}

//Ejecuta el algoritmo
function ejecutarAlgoritmo() {
  FlowManager.startFlow();
  FlowManager.startStep();
  FlowManager.selectSourceLine(2);
  FlowManager.finishStep();

  //Rellenamos C
  var vectC = new Array();
  var vectE = new Array();
  var vectP = new Array();
  var auxPesos = new Array();

  FlowManager.startStep();
  FlowManager.selectSourceLine(7);

  for (var i = 0; i < 5; i++) {
    for (var j = 0; j < 5; j++) {
      if (ejemploRobot1[i][j] == "R") {
        continue;
      } else if (ejemploRobot1[i][j] == "O") {
        auxPesos.push(9999999);
      } else if (ejemploRobot1[i][j] == "S") {
        auxPesos.push(0);
      } else {
        auxPesos.push(eval(ejemploRobot1[i][j]));
      }

      vectC.push(i * 5 + j + 1);
      vectE.push("Ø");
      vectP.push("Ø");
    }
  }
  FlowManager.oldResultValue("resultadosVectC", "Ø");
  FlowManager.newResultValue("resultadosVectC", Utilities.printArray(vectC));
  FlowManager.oldResultValue("resultadosEspecial", "Ø");
  FlowManager.newResultValue("resultadosEspecial", Utilities.printArray(vectE));
  FlowManager.oldResultValue("resultadosPredecesor", "Ø");
  FlowManager.newResultValue(
    "resultadosPredecesor",
    Utilities.printArray(vectP)
  );
  FlowManager.finishStep();
  FlowManager.startStep();
  FlowManager.setGraphStyle("grafoSalida", idRobot, "seleccionado");

  var index = 0;

  for (var i = 0; i < 5; i++) {
    for (var j = 0; j < 5; j++) {
      if (i == iR && j == jR) {
        continue;
      }
      FlowManager.selectSourceLine(9);
      FlowManager.selectSourceLine(10);

      var idNodoActual = i * 5 + j + 1;
      var idArista = obtenerIdArista(idNodoActual, idRobot);
      FlowManager.setGraphStyle("grafoSalida", idNodoActual, "seleccionado");

      FlowManager.oldResultValue(
        "resultadosEspecial",
        Utilities.printArray(vectE)
      );
      FlowManager.oldResultValue(
        "resultadosPredecesor",
        Utilities.printArray(vectP)
      );

      if (grafo.checkIfExistElement(idArista)) {
        FlowManager.setGraphStyle("grafoSalida", idArista, "seleccionado");
        vectE[index] = ejemploRobot1[i][j];
      } else vectE[index] = "∞";

      vectP[index++] = idRobot;
      FlowManager.newResultValue(
        "resultadosEspecial",
        Utilities.printArray(vectE)
      );
      FlowManager.newResultValue(
        "resultadosPredecesor",
        Utilities.printArray(vectP)
      );

      FlowManager.finishStep();
      FlowManager.startStep();
      FlowManager.unsetGraphStyle("grafoSalida", idNodoActual, "seleccionado");
      if (grafo.checkIfExistElement(idArista))
        FlowManager.unsetGraphStyle("grafoSalida", idArista, "seleccionado");
    }
  }
  // FlowManager.selectSourceLine(12);
  FlowManager.finishStep();
  var v = new Array();

  //FlowManager.startStep();
  FlowManager.unsetGraphStyle("grafoSalida", idRobot, "seleccionado");
  FlowManager.setGraphStyle("grafoSalida", idRobot, "solucion");
  var vPred = -1;
  var v = -1;
  while (vectC.length > 0 && v != idMeta) {
    FlowManager.selectSourceLine(12);
    FlowManager.finishStep();
    FlowManager.startStep();
    FlowManager.selectSourceLine(13);
    var id;
    var minAux = 9999999999;
    for (var i = 0; i < vectC.length; i++) {
      var aux = vectC[i] > idRobot ? vectC[i] - 2 : vectC[i] - 1;
      if (vectE[aux] < minAux) {
        v = vectC[i];
        minAux = vectE[aux];
        id = aux;
      }
    }
    //cambiamos el camino
    if (vPred != -1) {
      eliminarCaminoPrevio(vPred, vectP);
    }
    vPred = v;
    ponerCaminoActual(v, vectP);
    FlowManager.setGraphStyle("grafoSalida", v, "seleccionado");
    //FlowManager.setGraphStyle('grafoSalida', obtenerIdArista(v, vectP[id]), 'solucion');

    FlowManager.finishStep();
    FlowManager.startStep();
    FlowManager.selectSourceLine(14);
    FlowManager.oldResultValue("resultadosVectC", Utilities.printArray(vectC));
    removeItemFromArr(vectC, v);
    FlowManager.newResultValue("resultadosVectC", Utilities.printArray(vectC));
    FlowManager.finishStep();
    FlowManager.startStep();
    FlowManager.selectSourceLine(15);
    FlowManager.finishStep();
    if (v == idMeta) {
      FlowManager.startStep();
      FlowManager.selectSourceLine(12);
      FlowManager.finishStep();
      FlowManager.startStep();
      continue;
    }
    FlowManager.startStep();
    FlowManager.selectSourceLine(16);
    FlowManager.finishStep();
    FlowManager.startStep();
    for (var i = 0; i < vectC.length; i++) {
      FlowManager.selectSourceLine(17);
      FlowManager.setGraphStyle("grafoSalida", vectC[i], "seleccionado");

      if (!grafo.checkIfExistElement(obtenerIdArista(v, vectC[i]))) {
        FlowManager.finishStep();
        FlowManager.startStep();
        FlowManager.unsetGraphStyle("grafoSalida", vectC[i], "seleccionado");
        continue;
      }
      FlowManager.setGraphStyle(
        "grafoSalida",
        obtenerIdArista(v, vectC[i]),
        "seleccionado"
      );
      FlowManager.finishStep();
      var aux2 = vectC[i] > idRobot ? vectC[i] - 2 : vectC[i] - 1;
      var costeV = eval(vectE[id] + "+" + auxPesos[aux2]);
      if (vectE[i] == "∞" || costeV < vectE[i]) {
        FlowManager.startStep();
        FlowManager.selectSourceLine(18);
        FlowManager.selectSourceLine(19);
        FlowManager.oldResultValue(
          "resultadosEspecial",
          Utilities.printArray(vectE)
        );
        FlowManager.oldResultValue(
          "resultadosPredecesor",
          Utilities.printArray(vectP)
        );

        vectE[i] = costeV;
        vectP[i] = v;

        FlowManager.newResultValue(
          "resultadosEspecial",
          Utilities.printArray(vectE)
        );
        FlowManager.newResultValue(
          "resultadosPredecesor",
          Utilities.printArray(vectP)
        );
        FlowManager.finishStep();
      }

      FlowManager.startStep();
      FlowManager.unsetGraphStyle("grafoSalida", vectC[i], "seleccionado");
      FlowManager.unsetGraphStyle(
        "grafoSalida",
        obtenerIdArista(v, vectC[i]),
        "seleccionado"
      );
    }

    FlowManager.unsetGraphStyle("grafoSalida", v, "seleccionado");
    FlowManager.setGraphStyle("grafoSalida", v, "solucion");
    FlowManager.finishStep();
  }
  FlowManager.startStep();
  FlowManager.selectSourceLine(24);
  FlowManager.finishStep();
  FlowManager.startStep();
  FlowManager.selectSourceLine(25);
  FlowManager.finishStep();
  FlowManager.finishFlow();
}

function eliminarCaminoPrevio(v, vectP) {
  while (v != idRobot) {
    var aux = v > idRobot ? vectP[v - 2] : vectP[v - 1];
    FlowManager.unsetGraphStyle("grafoSalida", v, "solucion");
    FlowManager.unsetGraphStyle(
      "grafoSalida",
      obtenerIdArista(v, aux),
      "solucion"
    );
    v = aux;
  }
}

function ponerCaminoActual(v, vectP) {
  while (v != idRobot) {
    var aux = v > idRobot ? vectP[v - 2] : vectP[v - 1];
    FlowManager.setGraphStyle("grafoSalida", v, "solucion");
    FlowManager.setGraphStyle(
      "grafoSalida",
      obtenerIdArista(v, aux),
      "solucion"
    );
    v = aux;
  }
}

function removeItemFromArr(arr, item) {
  var i = arr.indexOf(item);

  if (i !== -1) {
    arr.splice(i, 1);
  }
}

function obtenerIdArista(nodo1, nodo2) {
  var idArista;
  if (nodo1 > nodo2) idArista = nodo2 + "-" + nodo1;
  else idArista = nodo1 + "-" + nodo2;
  return idArista;
}

//Inicia las variables
function inicializarZonaVariables() {
  var tabla = new InputTable(
    "entradaTabla",
    "Circuito",
    "Circuito por el que se desplazara el robot (R) hasta la meta (S), con los obstaculos (O) y el coste de paso por una casilla indicado.",
    TABLEHEADERTYPE.NUMBER,
    null,
    TABLEHEADERTYPE.NUMBER,
    null,
    ejemploRobot1
  );
  tabla.setStatic(true);
  InputData.addControl(tabla);

  InputData.onUpdate(actualizarDatos);
}

//Inicia para este caso la tabla, el objetos de volumen y el beneficio
function inicializarZonaResultados() {
  grafo = new Graph(
    "grafoSalida",
    "Grafo del circuito",
    "Grafo del circuito por el que se desplazara el robot (R) hasta la meta (S), con los obstaculos (O) y el coste de paso por una casilla indicado."
  );

  OutputData.addControl(grafo);
  var estilosGrafo = [
    // the stylesheet for the graph
    {
      selector: "node",
      style: {
        "background-color": "#666",
        label: "data(label)",
      },
    },
    {
      selector: "edge",
      style: {
        width: 3,
        "line-color": "#ccc",
        "target-arrow-color": "#ccc",
        "target-arrow-shape": "triangle",
      },
    },
    {
      selector: ".obstaculo",
      style: {
        "background-color": "red",
      },
    },
    {
      selector: ".inicio",
      style: {
        "background-color": "blue",
      },
    },
    {
      selector: ".meta",
      style: {
        "background-color": "green",
        "line-color": "green",
      },
    },
    {
      selector: ".seleccionado",
      style: {
        "border-color": "yellow",
        "border-width": 5,
        "line-color": "yellow",
      },
    },
    {
      selector: ".solucion",
      style: {
        "border-color": "green",
        "border-width": 5,
        "line-color": "green",
      },
    },
  ];

  grafo.initGraph(estilosGrafo);

  insertarDatosGrafo();

  // ponerEstilo(grafo, '0404-0503', 'meta');
  // ponerEstilo(grafo, '0402-0503', 'meta');
  // ponerEstilo(grafo, '0301-0402', 'meta');
  // ponerEstilo(grafo, '0202-0301', 'meta');
  // ponerEstilo(grafo, '0102-0202', 'meta');

  //
  //        style: [ // the stylesheet for the graph
  //          {
  //            selector: 'node',
  //            style: {
  //              'background-color': '#666',
  //              'label': 'data(id)'
  //            }
  //          },
  //
  //          {
  //            selector: 'edge',
  //            style: {
  //              'width': 3,
  //              'line-color': '#ccc',
  //              'target-arrow-color': '#ccc',
  //              'target-arrow-shape': 'triangle'
  //            }
  //          }
  //        ],
  //
  //      });
  //

  //var vectS = resultadoDeVariable('Vector C:', 'Nodos ya seleccionados.', 'resultadosVectS');
  var vectC = new OutputTextBox(
    "resultadosVectC",
    "Vector C",
    "Nodos no seleccionados.",
    "Ø"
  );
  OutputData.addControl(vectC);
  var especial = new OutputTextBox(
    "resultadosEspecial",
    "Vector especial",
    "Longitud del camino especial más corto (si el nodo está en S), o el camino más corto conocido (si el nodo está en C).",
    "Ø"
  );
  OutputData.addControl(especial);
  var predecesor = new OutputTextBox(
    "resultadosPredecesor",
    "Vector predecesor",
    "Identificador del nodo que precede al nodo i-ésimo en el camino más corto desde el origen.",
    "Ø"
  );
  OutputData.addControl(predecesor);
}

//actualiza los nuevos datos al pulsar el boton actualizar
function actualizarDatos() {
  ejemploRobot1 = InputData.get("entradaTabla").getData();
  //inicializarZonaVariables();
  insertarDatosGrafo();
  ejecutarAlgoritmo();
}

function insertarDatosGrafo() {
  var grafo = OutputData.get("grafoSalida");

  grafo.clearData();

  //creamos los nodos
  for (var i = 0; i < 5; i++) {
    for (var j = 0; j < 5; j++) {
      var id = i * 5 + j + 1;
      var label = "Coste: " + ejemploRobot1[i][j] + " Id: " + id;
      grafo.addNode(id, label);
      if (ejemploRobot1[i][j] == "O") grafo.setStyleToElement(id, "obstaculo");
      else if (ejemploRobot1[i][j] == "S") {
        grafo.setStyleToElement(id, "meta");
        iM = i;
        jM = j;
        idMeta = id;
      } else if (ejemploRobot1[i][j] == "R") {
        grafo.setStyleToElement(id, "inicio");
        iR = i;
        jR = j;
        idRobot = id;
      }
    }
  }

  //creamos las aristas
  for (var i = 0; i < 5; i++) {
    for (var j = 0; j < 5; j++) {
      if (ejemploRobot1[i][j] == "O") continue;

      var id = i * 5 + j + 1;
      var idArista = id + "-";
      if (i + 1 < 5) {
        if (ejemploRobot1[i + 1][j] != "O")
          grafo.addEdge(
            idArista + ((i + 1) * 5 + j + 1),
            id,
            (i + 1) * 5 + j + 1,
            null
          );
        if (j - 1 >= 0 && ejemploRobot1[i + 1][j - 1] != "O") {
          grafo.addEdge(
            idArista + ((i + 1) * 5 + j),
            id,
            (i + 1) * 5 + j,
            null
          );
        }
        if (j + 1 < 5 && ejemploRobot1[i + 1][j + 1] != "O") {
          grafo.addEdge(
            idArista + ((i + 1) * 5 + j + 2),
            id,
            (i + 1) * 5 + j + 2,
            null
          );
        }
      }
      if (j + 1 < 5 && ejemploRobot1[i][j + 1] != "O") {
        grafo.addEdge(idArista + (i * 5 + j + 2), id, i * 5 + j + 2, null);
      }
    }
  }

  var gl = new GridLayout(5, 5);
  grafo.setLayout(gl);
}

// Usado para pintar el algoritmo de dijkstra
function codigoAlgoritmoAuxiliar() {
  SourceCode.printAuxLineCode(
    "tipo VectorNat = matriz[O..n] de natural",
    0,
    "algoritmoAux"
  );
  SourceCode.printAuxLineCode(
    "fun Dijkstra (G = <N,A>: grafo): VectorNat, VectorNat",
    0,
    "algoritmoAux"
  );
  SourceCode.printAuxLineCode("var", 1, "algoritmoAux");
  SourceCode.printAuxLineCode(
    "especial, predecesor: VectorNat",
    2,
    "algoritmoAux"
  );
  SourceCode.printAuxLineCode("C: conjunto de nodos", 2, "algoritmoAux");
  SourceCode.printAuxLineCode("fvar", 1, "algoritmoAux");
  SourceCode.printAuxLineCode("C={2,3,...,n}", 1, "algoritmoAux");
  SourceCode.printAuxLineCode("para i = 2 hasta n hacer", 1, "algoritmoAux");
  SourceCode.printAuxLineCode(
    "especial[i] = Distancia(1 ,i)",
    2,
    "algoritmoAux"
  );
  SourceCode.printAuxLineCode("predecesor[i] = 1", 2, "algoritmoAux");
  SourceCode.printAuxLineCode("fpara", 1, "algoritmoAux");
  SourceCode.printAuxLineCode(
    "mientras C contenga más de 1 nodo hacer",
    1,
    "algoritmoAux"
  );
  SourceCode.printAuxLineCode(
    "v = nodo en C que minimiza especial[v]",
    2,
    "algoritmoAux"
  );
  SourceCode.printAuxLineCode("C = C{v}", 2, "algoritmoAux");
  SourceCode.printAuxLineCode("para cada w en C hacer", 2, "algoritmoAux");
  SourceCode.printAuxLineCode(
    "si especial[w] > especial[v] + Distancia(v,w) entonces",
    3,
    "algoritmoAux"
  );
  SourceCode.printAuxLineCode(
    "especial[w] = especial[v] + Distancia(v,w)",
    4,
    "algoritmoAux"
  );
  SourceCode.printAuxLineCode("predesecor[w] = v", 4, "algoritmoAux");
  SourceCode.printAuxLineCode("fsi", 3, "algoritmoAux");
  SourceCode.printAuxLineCode("fpara", 2, "algoritmoAux");
  SourceCode.printAuxLineCode("fmientras", 1, "algoritmoAux");
  SourceCode.printAuxLineCode("dev especial[],predecesor[]", 1, "algoritmoAux");
  SourceCode.printAuxLineCode("ffun", 0, "algoritmoAux");
}
